#include "mainworker.h"


extern "C" {

#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
#include "libswscale/swscale.h"
#include "libavutil/imgutils.h"
#include "libavutil/hwcontext.h"

}


#include <QThread>
#include <QFile>
#include <QSettings>





class MainWorkerPrivate
{
public:
    explicit MainWorkerPrivate(OpenGLDisplayNV12* display);


    bool init();

    bool circle();

    void release();




    QString const               k_settings_filename;
    QString                     m_filename;
    AVHWDeviceType              m_hwdevice_type;
    AVBufferRef*                m_hwdevice_context;
    AVFormatContext*            m_format_context;
    AVPixelFormat               m_pix_format;
    int                         m_video_stream;
    AVCodecContext*             m_codec_context;
    AVCodec*                    m_codec;
    AVPacket                    m_packet;
    AVFrame*                    m_frame;
    AVFrame*                    m_frame_nv12;

    OpenGLDisplayNV12*          m_display;

    volatile bool               m_stop;
};



MainWorkerPrivate::MainWorkerPrivate(OpenGLDisplayNV12* display)
    : k_settings_filename("params.ini")
    , m_hwdevice_type(AV_HWDEVICE_TYPE_NONE)
    , m_hwdevice_context(nullptr)
    , m_format_context(nullptr)
    , m_codec_context(nullptr)
    , m_codec(nullptr)
    , m_frame(nullptr)
    , m_frame_nv12(nullptr)
    , m_display(display)
    , m_stop(false) {}



template <AVPixelFormat pix_fmt>
static inline AVPixelFormat get_hw_format(AVCodecContext* ctx, const AVPixelFormat* pix_fmts)
{
    const AVPixelFormat* p;
    for (p = pix_fmts; *p != -1; p++) {
        if (*p == pix_fmt) {
            return *p;
        }
    }
    return AV_PIX_FMT_NONE;
}


bool MainWorkerPrivate::init()
{
    if (!QFile::exists(k_settings_filename)) {
        return false;
    }

    QSettings settings(k_settings_filename, QSettings::IniFormat);
    m_filename = settings.value("filename").toString();
    m_hwdevice_type = static_cast<AVHWDeviceType>(settings.value("hwdevice").toInt());


    avformat_network_init();

    int err = avformat_open_input(&m_format_context, m_filename.toLatin1().data(), nullptr, nullptr);
    if (err) {
        return false;
    }

    err = avformat_find_stream_info(m_format_context, nullptr);
    if (err < 0) {
        return false;
    }

    av_dump_format(m_format_context, 0, m_filename.toLatin1().data(), 0);

    m_video_stream = av_find_best_stream(m_format_context, AVMEDIA_TYPE_VIDEO, -1, -1, &m_codec, 0);
    if (m_video_stream < 0) {
        return false;
    }

    for (int i = 0; ; i++) {
        AVCodecHWConfig const* config = avcodec_get_hw_config(m_codec, i);
        if (!config) {
            return false;
        }
        if (config->methods & AV_CODEC_HW_CONFIG_METHOD_HW_DEVICE_CTX &&
                config->device_type == m_hwdevice_type) {
            m_pix_format = config->pix_fmt;
            break;
        }
    }

    m_codec_context = avcodec_alloc_context3(m_codec);
    if (m_codec_context == nullptr) {
        return false;
    }

    AVCodecParameters* codec_params = m_format_context->streams[m_video_stream]->codecpar;

    err = avcodec_parameters_to_context(m_codec_context, codec_params);
    if (err) {
        return false;
    }

    // --== HW DECODER ==--
    switch (m_pix_format) {
    case AV_PIX_FMT_VAAPI:
        m_codec_context->get_format = get_hw_format<AV_PIX_FMT_VAAPI>;
        break;
    case AV_PIX_FMT_DXVA2_VLD:
        m_codec_context->get_format = get_hw_format<AV_PIX_FMT_DXVA2_VLD>;
        break;
    case AV_PIX_FMT_VDPAU:
        m_codec_context->get_format = get_hw_format<AV_PIX_FMT_VDPAU>;
        break;
    case AV_PIX_FMT_CUDA:
        m_codec_context->get_format = get_hw_format<AV_PIX_FMT_CUDA>;
        break;
    default:
        return false;
        ;
    }

    err = av_hwdevice_ctx_create(&m_hwdevice_context, m_hwdevice_type, nullptr, nullptr, 0);
    if (err < 0) {
        return false;
    }

    m_codec_context->hw_device_ctx = av_buffer_ref(m_hwdevice_context);
    // --== END HW DECODER ==--

    err = avcodec_open2(m_codec_context, m_codec, nullptr);
    if (err < 0) {
        return false;
    }

    m_frame = av_frame_alloc();
    if (m_frame == nullptr) {
        return false;
    }

    m_frame_nv12 = av_frame_alloc();
    if (m_frame_nv12 == nullptr) {
        return false;
    }

    return true;
}


bool MainWorkerPrivate::circle()
{
    int err = av_read_frame(m_format_context, &m_packet);
    if (err < 0) {
        return false;
    }

    if (m_packet.stream_index == m_video_stream) {
        err = avcodec_send_packet(m_codec_context, &m_packet);
        if (err < 0) {
            return false;
        }
        while (true) {
            err = avcodec_receive_frame(m_codec_context, m_frame);
            if (err == AVERROR(EAGAIN) || err == AVERROR_EOF) {
                break;
            }
            else if (err < 0) {
                return false;
            }
            if (m_frame->format == m_pix_format) {
                err = av_hwframe_transfer_data(m_frame_nv12, m_frame, 0);
                if (err < 0) {
                    return false;
                }
                m_display->DisplayVideoFrame(m_frame_nv12->data, m_codec_context->width, m_codec_context->height);
            }

        }
    }
    av_packet_unref(&m_packet);
    return true;
}


void MainWorkerPrivate::release()
{
    av_frame_free(&m_frame_nv12);

    av_frame_free(&m_frame);

    avcodec_close(m_codec_context);

    avformat_close_input(&m_format_context);

}







MainWorker::MainWorker(OpenGLDisplayNV12* display)
    : d_ptr(new MainWorkerPrivate(display)) {}




void MainWorker::run()
{
    Q_D(MainWorker);
    if (d->init()) {
        while (d->circle() && !d->m_stop) {}
        d->release();
    }
    emit finished();
}


void MainWorker::stop()
{
    Q_D(MainWorker);
    d->m_stop = true;
}

