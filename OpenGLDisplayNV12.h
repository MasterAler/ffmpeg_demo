#pragma once

#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QScopedPointer>
#include <QException>

QT_FORWARD_DECLARE_CLASS(QCloseEvent)

class OpenGLDisplayNV12 : public QOpenGLWidget, public QOpenGLFunctions
{
    Q_OBJECT
public:
    explicit OpenGLDisplayNV12(QWidget* parent = nullptr);
    ~OpenGLDisplayNV12() override;

    void DisplayVideoFrame(unsigned char **data, int frameWidth, int frameHeight);

protected:
    void initializeGL() override;
    void resizeGL(int w, int h) override;
    void paintGL() override;

    void closeEvent(QCloseEvent* e) override;

signals:
    Q_SIGNAL void closed();

private:
    struct OpenGLDisplayNV12Impl;
    QScopedPointer<OpenGLDisplayNV12Impl> impl;
};
