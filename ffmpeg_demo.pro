QT += core gui widgets

TARGET = ffmpeg_demo
CONFIG += console
CONFIG += c++11
CONFIG -= app_bundle

TEMPLATE = app

CONFIG(release, debug|release) {
  BUILD_DIR = $$PWD/bin/release
}

CONFIG(debug, debug|release) {
  BUILD_DIR = $$PWD/bin/debug
}

DESTDIR = $$BUILD_DIR

INCLUDEPATH += $$PWD/ffmpeg/include/

LIBS += \
    -L$$PWD/ffmpeg/lib/ \
    -lavcodec \
    -lavformat \
    -lavdevice \
    -lavutil \
    -lswscale


SOURCES += main.cpp \
    mainworker.cpp \
    OpenGLDisplayRGB.cpp \
    OpenGLDisplayYUV.cpp \
    OpenGLDisplayNV12.cpp

HEADERS += \
    mainworker.h \
    OpenGLDisplayRGB.h \
    OpenGLDisplayYUV.h \
    OpenGLDisplayNV12.h

RESOURCES += \
    resource.qrc

win32 {
    ffmpeg.path += $$shell_quote($$BUILD_DIR)
    ffmpeg.files += $$PWD/ffmpeg/bin/*.dll
    INSTALLS += ffmpeg
}
