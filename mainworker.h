#ifndef MAINWORKER_H
#define MAINWORKER_H

#include "OpenGLDisplayNV12.h"

#include <QObject>
#include <QRunnable>
#include <QSharedPointer>





class MainWorkerPrivate;

class MainWorker : public QObject, public QRunnable
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(MainWorker)


public:
    explicit MainWorker(OpenGLDisplayNV12* display);


    void run() override;


    Q_SLOT void stop();

    Q_SIGNAL void finished();



private:
    QSharedPointer<MainWorkerPrivate>       d_ptr;





};


#endif // MAINWORKER_H
