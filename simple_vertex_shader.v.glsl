attribute vec4 vertexIn;
attribute vec2 textureIn;
varying vec2 vTextureCoord;

void main(void)
{
    gl_Position = vertexIn;
    vTextureCoord = textureIn;
}
