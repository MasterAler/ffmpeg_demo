#include "OpenGLDisplayNV12.h"
#include "mainworker.h"

#include <QApplication>
#include <QThreadPool>


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    auto display = new OpenGLDisplayNV12;
    display->resize(800, 450);
    display->setWindowTitle("Look at me!");
    display->show();

    MainWorker* main_worker = new MainWorker(display);
    QObject::connect(display, &OpenGLDisplayNV12::closed, main_worker, &MainWorker::stop);
    QObject::connect(main_worker, &MainWorker::finished, display, &OpenGLDisplayNV12::deleteLater);
    QObject::connect(main_worker, &MainWorker::finished, &a, &QApplication::quit);
    QThreadPool::globalInstance()->start(main_worker);


    return a.exec();
}

