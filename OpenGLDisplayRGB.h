#pragma once


#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QScopedPointer>
#include <QException>


/*!
 * \brief The OpenGLDisplay class
 * Simple OpenGL display, that renders RGBA to texture
 */
class OpenGLDisplayRGB : public QOpenGLWidget, public QOpenGLFunctions
{
    Q_OBJECT


public:
    explicit OpenGLDisplayRGB(QWidget* parent = nullptr);
    ~OpenGLDisplayRGB() override;


protected:
    void initializeGL() override;
    void resizeGL(int w, int h) override;
    void paintGL() override;

    void closeEvent(QCloseEvent* e) override;


public:
    void DisplayVideoFrame(unsigned char* data, int frameWidth, int frameHeight);


    Q_SIGNAL void closed();



private:
    struct OpenGLDisplayRGBImpl;
    QScopedPointer<OpenGLDisplayRGBImpl> impl;
};
